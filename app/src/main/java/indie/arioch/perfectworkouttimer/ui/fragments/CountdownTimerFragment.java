package indie.arioch.perfectworkouttimer.ui.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import indie.arioch.perfectworkouttimer.MainActivity.SectionsPagerAdapter;
import indie.arioch.perfectworkouttimer.R;

/**
 * Created by ariochdivij666 on 2/9/16.
 */
public class CountdownTimerFragment extends Fragment implements SensorEventListener {

    static int Timer_Value_In_Milli_Seconds = 60000;
    static int Timer_Tick_Interval = 1000;
    static int Minute = 60;
    static int Milli_Seconds_30 = 30;
    private static int Milli_Seconds_10 = 10000;
    private static int Milli_Seconds_5 = 5000;


    private SensorManager sensorManager;
    private Sensor proximitySensor;

    private AudioManager audioManager;


    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    CountDownTimer countDownTimer;
    private boolean startedTimer;
    private boolean loadedSound;
    private SoundPool soundPool;
    private int beepSound;
    private int airHornSound;


    public CountdownTimerFragment(){

    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static CountdownTimerFragment newInstance(int sectionNumber) {
        CountdownTimerFragment fragment = new CountdownTimerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.sensorManager = ((SensorManager) this.getContext().getSystemService(Context.SENSOR_SERVICE));
        this.proximitySensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        final TextView textView = (TextView) rootView.findViewById(R.id.textViewTimer);

        this.audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {

            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                CountdownTimerFragment.this.loadedSound = true;
            }
        });

        this.beepSound = soundPool.load(this.getContext(), R.raw.beep, 1);
        this.airHornSound = soundPool.load(this.getContext(), R.raw.airhorn, 2);

        textView.setText(this.timerStringValue());

        this.countDownTimer = new CountDownTimer(CountdownTimerFragment.Timer_Value_In_Milli_Seconds, CountdownTimerFragment.Timer_Tick_Interval) {
            @Override
            public void onTick(long millisUntilFinished) {
                CountdownTimerFragment.Timer_Value_In_Milli_Seconds -= CountdownTimerFragment.Timer_Tick_Interval;
                textView.setText(CountdownTimerFragment.this.timerStringValue());
                if (CountdownTimerFragment.Timer_Value_In_Milli_Seconds == Milli_Seconds_30 ||
                        CountdownTimerFragment.Timer_Value_In_Milli_Seconds == CountdownTimerFragment.Milli_Seconds_10 ||
                        (CountdownTimerFragment.Timer_Value_In_Milli_Seconds < CountdownTimerFragment.Milli_Seconds_5 &&
                        CountdownTimerFragment.Timer_Value_In_Milli_Seconds > 0)) {

                    CountdownTimerFragment.this.soundPool.play(CountdownTimerFragment.this.beepSound, CountdownTimerFragment.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) , CountdownTimerFragment.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);

                } else if(CountdownTimerFragment.Timer_Value_In_Milli_Seconds > 0) {


                }
            }

            @Override
            public void onFinish() {
                CountdownTimerFragment.Timer_Value_In_Milli_Seconds = 60000;
                CountdownTimerFragment.this.pauseTimer();
                CountdownTimerFragment.this.soundPool.play(CountdownTimerFragment.this.airHornSound, CountdownTimerFragment.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) , CountdownTimerFragment.this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
                textView.setText(CountdownTimerFragment.this.timerStringValue());
            }
        };

        return rootView;
    }


    private String timerStringValue() {

        StringBuilder stringBuilder = new StringBuilder("");

        int currentTime = CountdownTimerFragment.Timer_Value_In_Milli_Seconds;
        currentTime /= CountdownTimerFragment.Timer_Tick_Interval;
        int minutes = currentTime / CountdownTimerFragment.Minute;

        //Currently hard coding the time as 1 min.
        if(minutes == 0) {
            stringBuilder.append("00:");
        } else if (minutes % 10 == 0){
            stringBuilder.append("0");
            stringBuilder.append(minutes);
            stringBuilder.append(" : ");
        }

        stringBuilder.append(currentTime % CountdownTimerFragment.Minute);

        return stringBuilder.toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(this.proximitySensor !=null && event.sensor.getType() == this.proximitySensor.getType())
        {
            this.detectProximity(event);
        }
    }

    private void detectProximity(SensorEvent event) {
        if(!this.startedTimer && event.values[0] == 0)
        {
            if(this.loadedSound)
            {
                this.soundPool.play(this.beepSound, this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) , this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
            }
            this.startedTimer = true;
            this.countDownTimer.start();
            Log.i("detectProximity", "timer started");
        }
        else if(this.startedTimer && event.values[0] == 0)
        {
            if(this.loadedSound)
            {
                this.soundPool.play(this.beepSound, this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) , this.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 1, 0, 1f);
            }
            this.pauseTimer();
            Log.i("detectProximity", "timer paused");
        }
    }

    private void pauseTimer() {
        this.startedTimer = false;
        this.countDownTimer.cancel();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}